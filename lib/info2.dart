import 'package:flutter/material.dart';

class info2 extends StatelessWidget {
  const info2({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height * 0.77,
      child: Column(
        children: [
          SizedBox(
            height: 30,
          ),
          Container(
            height: 90.0,
            child: ListView(
              scrollDirection: Axis.horizontal,
              children: [
                hourly(
                    time: 'Now',
                    temp: '22',
                    icon: Icon(
                      Icons.cloud,
                      color: Colors.grey[300],
                    ),
                    wind: '11.2km/h'),
                hourly(
                    time: '06:00',
                    temp: '24',
                    icon: Icon(
                      Icons.cloud,
                      color: Colors.grey[300],
                    ),
                    wind: '12.2km/h'),
                hourly(
                    time: '07:00',
                    temp: '25',
                    icon: Icon(
                      Icons.cloud,
                      color: Colors.grey[300],
                    ),
                    wind: '15.2km/h'),
                hourly(
                    time: '08:00',
                    temp: '24',
                    icon: Icon(
                      Icons.sunny,
                      color: Colors.orangeAccent,
                    ),
                    wind: '16.8km/h'),
                hourly(
                    time: '09:00',
                    temp: '26',
                    icon: Icon(
                      Icons.sunny,
                      color: Colors.orangeAccent,
                    ),
                    wind: '16.6km/h'),
                hourly(
                    time: '10:00',
                    temp: '26',
                    icon: Icon(
                      Icons.sunny,
                      color: Colors.orangeAccent,
                    ),
                    wind: '15.2km/h'),
                hourly(
                    time: '11:00',
                    temp: '26',
                    icon: Icon(
                      Icons.sunny,
                      color: Colors.orangeAccent,
                    ),
                    wind: '15.2km/h'),
                hourly(
                    time: '12:00',
                    temp: '26',
                    icon: Icon(
                      Icons.sunny,
                      color: Colors.orangeAccent,
                    ),
                    wind: '15.2km/h'),
                hourly(
                    time: '13:00',
                    temp: '26',
                    icon: Icon(
                      Icons.sunny,
                      color: Colors.orangeAccent,
                    ),
                    wind: '15.2km/h'),
              ],
            ),
          ),
          SizedBox(height: 25),
          Container(
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(15),
                color: Color.fromARGB(212, 183, 183, 183)),
            width: MediaQuery.of(context).size.width * 0.90,
            height: 350,
            child: card1Info(),
          ),
          SizedBox(height: 25),
          Container(
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(15),
                color: Color.fromARGB(212, 183, 183, 183)),
            width: MediaQuery.of(context).size.width * 0.90,
            height: 80,
            child: card2Info(),
          ),
        ],
      ),
    );
  }
}

Widget hourly({time: String, temp: String, icon: Icon, wind: String}) {
  return Padding(
    padding: const EdgeInsets.symmetric(horizontal: 20),
    child: Container(
      width: 65,
      child: Column(
        children: [
          Text(
            '$time',
            style: TextStyle(color: Colors.white, fontSize: 15),
          ),
          Text(
            '$temp°',
            style: TextStyle(
                color: Colors.white, fontSize: 18, fontWeight: FontWeight.bold),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 5),
            child: icon,
          ),
          Text(
            '$wind',
            style: TextStyle(color: Colors.white, fontSize: 13),
          ),
        ],
      ),
    ),
  );
}

Widget card2Info() {
  return Padding(
    padding: const EdgeInsets.only(left: 20, top: 15),
    child: Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text(
          'Air Quality Index',
          style: TextStyle(fontSize: 13, color: Colors.grey[100]),
        ),
        SizedBox(height: 0),
        Row(
          children: [
            Icon(
              Icons.energy_savings_leaf,
              color: Colors.white,
            ),
            SizedBox(width: 5),
            Text(
              '45',
              style: TextStyle(
                  fontSize: 20,
                  color: Colors.white,
                  fontWeight: FontWeight.bold),
            ),
            SizedBox(width: 100),
            TextButton(
              onPressed: () {},
              child: Text(
                'Full air quality forecast >',
                style: TextStyle(
                  fontSize: 15,
                  color: Colors.white,
                ),
              ),
            )
          ],
        ),
      ],
    ),
  );
}

Widget card1Info() {
  return Column(
    children: [
      Padding(
        padding: const EdgeInsets.symmetric(vertical: 15),
        child: Container(
            width: 350, height: 90, color: Colors.transparent, child: Text('')),
      ),
      Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          infoLayer1(),
          SizedBox(height: 20),
          infoLayer2(),
          SizedBox(height: 20),
          infoLayer3()
        ],
      ),
    ],
  );
}

Widget infoLayer1() {
  return Row(
    children: [
      Padding(
        padding: const EdgeInsets.only(left: 20, right: 120),
        child: Container(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Text(
                'Real feel',
                style: TextStyle(fontSize: 13, color: Colors.grey[100]),
              ),
              SizedBox(height: 7),
              Text(
                '21\u2103',
                style: TextStyle(
                    fontSize: 23,
                    color: Colors.white,
                    fontWeight: FontWeight.bold),
              )
            ],
          ),
        ),
      ),
      Container(
        child: Column(
          children: [
            Text(
              'Humidity',
              style: TextStyle(fontSize: 13, color: Colors.grey[100]),
            ),
            SizedBox(height: 7),
            Text(
              '66%',
              style: TextStyle(
                  fontSize: 23,
                  color: Colors.white,
                  fontWeight: FontWeight.bold),
            )
          ],
        ),
      ),
    ],
  );
}

Widget infoLayer2() {
  return Row(
    children: [
      Padding(
        padding: const EdgeInsets.only(left: 20, right: 86),
        child: Container(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Text(
                'Chance of rain',
                style: TextStyle(fontSize: 13, color: Colors.grey[100]),
              ),
              SizedBox(height: 7),
              Text(
                '40%',
                style: TextStyle(
                    fontSize: 23,
                    color: Colors.white,
                    fontWeight: FontWeight.bold),
              ),
            ],
          ),
        ),
      ),
      Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Text(
              'Pressure',
              style: TextStyle(fontSize: 13, color: Colors.grey[100]),
            ),
            SizedBox(height: 7),
            Text(
              '1015mbar',
              style: TextStyle(
                  fontSize: 23,
                  color: Colors.white,
                  fontWeight: FontWeight.bold),
            )
          ],
        ),
      ),
    ],
  );
}

Widget infoLayer3() {
  return Row(
    children: [
      Padding(
        padding: const EdgeInsets.only(left: 20, right: 84),
        child: Container(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Text(
                'Wind speed',
                style: TextStyle(fontSize: 13, color: Colors.grey[100]),
              ),
              SizedBox(height: 7),
              Text(
                '5.8km/h',
                style: TextStyle(
                    fontSize: 23,
                    color: Colors.white,
                    fontWeight: FontWeight.bold),
              )
            ],
          ),
        ),
      ),
      Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Text(
              'UV index',
              style: TextStyle(fontSize: 13, color: Colors.grey[100]),
            ),
            SizedBox(height: 7),
            Text(
              '0',
              style: TextStyle(
                  fontSize: 23,
                  color: Colors.white,
                  fontWeight: FontWeight.bold),
            )
          ],
        ),
      ),
    ],
  );
}
