import 'package:flutter/material.dart';

class info1 extends StatelessWidget {
  const info1({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height * 0.77,
      // color: Colors.red,
      child: Column(
        children: [
          Container(
            child: Column(
              children: [
                SizedBox(height: 120),
                Center(
                  child: Text(
                    '25\u2103',
                    style: TextStyle(
                      fontSize: 90,
                      color: Color.fromARGB(255, 230, 230, 230),
                    ),
                  ),
                ),
                Center(
                  child: Text(
                    'Rainy',
                    style: TextStyle(
                      fontSize: 25,
                      color: Color.fromARGB(255, 230, 230, 230),
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 15),
                  child: Container(
                    width: 100,
                    height: 30,
                    child: ElevatedButton(
                        style: ElevatedButton.styleFrom(
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(30.0),
                          ),
                          backgroundColor: Color.fromARGB(212, 183, 183, 183),
                        ),
                        onPressed: () {},
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Padding(
                              padding: const EdgeInsets.only(right: 8),
                              child: Icon(Icons.energy_savings_leaf, size: 20),
                            ),
                            Text(
                              'AQI 45',
                              style: TextStyle(fontSize: 13),
                            ),
                          ],
                        )),
                  ),
                ),
              ],
            ),
          ),
          SizedBox(height: 25),
          Padding(
            padding: const EdgeInsets.only(left: 25, right: 25, top: 60),
            child: Container(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  moreDetails(),
                  dailyInfo1(),
                  dailyInfo2(),
                  dailyInfo3()
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}

Widget moreDetails() {
  return Row(
    mainAxisAlignment: MainAxisAlignment.end,
    children: [
      TextButton(
        onPressed: () {},
        child: Text(
          textAlign: TextAlign.right,
          'More details >',
          style: TextStyle(
            fontSize: 13,
            color: Colors.white,
          ),
        ),
      ),
    ],
  );
}

Widget dailyInfo1() {
  return Padding(
    padding: const EdgeInsets.only(bottom: 8),
    child: Container(
      child: Row(
        children: [
          Padding(
            padding: const EdgeInsets.only(right: 5),
            child: Icon(
              Icons.cloudy_snowing,
              color: Colors.grey,
              size: 35,
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(right: 150),
            child: Text(
              'Yesterday-Rain',
              style: TextStyle(color: Colors.white, fontSize: 17),
            ),
          ),
          Text(
            '30°/20°',
            style: TextStyle(color: Colors.white, fontSize: 17),
          ),
        ],
      ),
    ),
  );
}

Widget dailyInfo2() {
  return Padding(
    padding: const EdgeInsets.symmetric(vertical: 8),
    child: Container(
      child: Row(
        children: [
          Padding(
            padding: const EdgeInsets.only(right: 5),
            child: Icon(
              Icons.cloud,
              color: Colors.grey[300],
              size: 35,
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(right: 160),
            child: Text(
              'Today-Cloudy',
              style: TextStyle(color: Colors.white, fontSize: 17),
            ),
          ),
          Text(
            '30°/22°',
            style: TextStyle(color: Colors.white, fontSize: 17),
          ),
        ],
      ),
    ),
  );
}

Widget dailyInfo3() {
  return Padding(
    padding: const EdgeInsets.symmetric(vertical: 8),
    child: Container(
      child: Row(
        children: [
          Padding(
            padding: const EdgeInsets.only(right: 5),
            child: Icon(
              Icons.wb_sunny,
              color: Colors.orangeAccent,
              size: 35,
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(right: 133),
            child: Text(
              'Tomorrow-Sunny',
              style: TextStyle(color: Colors.white, fontSize: 17),
            ),
          ),
          Text(
            '34°/25°',
            style: TextStyle(color: Colors.white, fontSize: 17),
          ),
        ],
      ),
    ),
  );
}
